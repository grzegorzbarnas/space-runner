using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Transform Target;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Target)
        {
            transform.position = Target.TransformPoint(new Vector3(0f, 10f, -10f));
            transform.LookAt(Target);
        }
    }
}