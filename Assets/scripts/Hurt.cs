using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurt : MonoBehaviour
{
     private void OnTriggerEnter(Collider other)
    {
        PlayerLife playerLife = other.GetComponent<PlayerLife>();
        if (playerLife != null)
        {
            playerLife.DecrementLife();
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
