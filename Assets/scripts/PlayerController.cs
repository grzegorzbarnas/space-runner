using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class PlayerController : MonoBehaviour
{
    [SerializeField] Vector3 angleVelocity = new Vector3(0, 60, 0);
    Rigidbody rb;
    Transform tr;

    public VirtualJoystick virtualJoystick;
    public VirtualJoystick virtualJoystick2;
    public Vector3 MoveVector { set; get; }
    public Vector3 MoveVector2 { set; get; }
    // Start is called before the first frame update

    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        tr = GetComponent<Transform>();
        PlayerInput playerInput = GetComponent<PlayerInput>();
        playerInput.OnDirectionInputDetected += Move;
        playerInput.OnRotationInputDetected += Rotate;

    }
    void Move(Vector3 movementVector)
    {
        rb.MovePosition(tr.position + tr.right * movementVector.x + tr.forward * movementVector.z);
    }
    void Rotate(float rotation)
    {
        if (rotation != 0f)
        {
            Quaternion delataRotation = Quaternion.Euler(angleVelocity * rotation * Time.fixedDeltaTime);
            rb.MoveRotation(rb.rotation * delataRotation);
        }
        else
        {
            Quaternion delataRotation = Quaternion.Euler(Vector3.zero * Time.fixedDeltaTime);
            rb.MoveRotation(rb.rotation * delataRotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveVector = virtualJoystickMove();
        Move(MoveVector);
        MoveVector2 = virtualJoystickMove2();
        Rotate(MoveVector2.x);
    }
    void Movement()
    {
        rb.MovePosition(tr.position + tr.right * MoveVector.x + tr.forward * MoveVector.z);
    }
    private Vector3 virtualJoystickMove()
    {
        Vector3 dir = Vector3.zero;

        dir.x = virtualJoystick.Horizontal();
        dir.z = virtualJoystick.Vertical();
        if (dir.magnitude > 1) dir.Normalize();
        return dir;
    }
    private Vector3 virtualJoystickMove2()
    {
        Vector3 dir = Vector3.zero;
        dir.x = virtualJoystick2.Horizontal();
        dir.z = virtualJoystick2.Vertical();
        if (dir.magnitude > 1) dir.Normalize();
        return dir;
    }
}

