using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    public UnityAction<Vector3> OnDirectionInputDetected;
    public UnityAction<float> OnRotationInputDetected;
    [SerializeField] float inputSensivity = 0.05f;
    [SerializeField] float gravity = 0.05f;
    [SerializeField] KeyCode forwardButton;
    [SerializeField] KeyCode backwardButton;
    [SerializeField] KeyCode rightButton;
    [SerializeField] KeyCode leftButton;
    [SerializeField] KeyCode rotateRightButton;
    [SerializeField] KeyCode rotateLeftButton;
    [SerializeField] KeyCode nitroButton;

    [SerializeField] float verticalMovment;
    [SerializeField] float horizontalMovment;
    [SerializeField] float rotation;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        VerticalInput();
        HorizontalInput();
        RotationInput();
        NitroInput();

        if (verticalMovment != 0f || horizontalMovment != 0f)
        {
            OnDirectionInputDetected?.Invoke(new Vector3(horizontalMovment, 0f, verticalMovment));
        }
        if (rotation != 0f)
        {
            OnRotationInputDetected?.Invoke(rotation);
        }
    }

    void HorizontalInput()
    {
        if (Input.GetKey(rightButton))
        {
            horizontalMovment += inputSensivity;
        }
        else if (Input.GetKey(leftButton))
        {
            horizontalMovment -= inputSensivity;
        }
        else if (horizontalMovment != 0f)
        {
            horizontalMovment = Mathf.Lerp(horizontalMovment, 0f, gravity * Time.deltaTime);
            horizontalMovment = Mathf.Abs(horizontalMovment) < inputSensivity ? 0f : horizontalMovment;
        }
        horizontalMovment = Mathf.Clamp(horizontalMovment, -1f, 1f);
    }

    void VerticalInput()
    {
        if (Input.GetKey(forwardButton))
        {
            verticalMovment += inputSensivity;
        }
        else if (Input.GetKey(backwardButton))
        {
            verticalMovment -= inputSensivity;
        }
        else if (verticalMovment != 0f)
        {
            verticalMovment = Mathf.Lerp(verticalMovment, 0f, gravity * Time.deltaTime);
            verticalMovment = Mathf.Abs(verticalMovment) < inputSensivity ? 0f : verticalMovment;
        }
        verticalMovment = Mathf.Clamp(verticalMovment, -1f, 1f);
    }
    void NitroInput()
    {
        if (Input.GetKey(nitroButton))
        {
            verticalMovment = verticalMovment * 2;
        }
    }

    void RotationInput()
    {
        if (Input.GetKey(rotateRightButton))
        {
            rotation += inputSensivity;
        }
        else if (Input.GetKey(rotateLeftButton))
        {
            rotation -= inputSensivity;
        }
        else if (rotation != 0f)
        {
            rotation = Mathf.Lerp(rotation, 0f, gravity * Time.deltaTime);
            rotation = Mathf.Abs(rotation) < inputSensivity ? 0f : rotation;
        }
        rotation = Mathf.Clamp(rotation, -1f, 1f);
    }
}
