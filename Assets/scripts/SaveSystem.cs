using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveToFile(SaveData saveData)
    {
        string destination = Application.persistentDataPath + "/save.dat";
        FileStream fileStream;

        if (File.Exists(destination)) fileStream = File.OpenWrite(destination);
        else fileStream=File.Create(destination);

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(fileStream, saveData);
        fileStream.Close();
    }
    public static SaveData LoadFromFile()
    {
       string destination = Application.persistentDataPath + "/save.dat";
       FileStream fileStream;

       if(File.Exists(destination))fileStream = File.OpenRead(destination);
       else 
       {
           Debug.Log("file not found");
           return null;
       }
       BinaryFormatter bf = new BinaryFormatter();
       SaveData data=(SaveData) bf.Deserialize(fileStream);
       fileStream.Close();
       return data;
    }
}
