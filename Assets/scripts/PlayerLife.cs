using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    public UnityAction<int> OnLifeChange;
    int playerLife = 3;
    public void DecrementLife()
    {
        if (playerLife > 0)
        {
            playerLife--;
            OnLifeChange?.Invoke(playerLife);
        }
        else
        {
            SceneManager.LoadScene("Prototype 1");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        OnLifeChange?.Invoke(playerLife);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
