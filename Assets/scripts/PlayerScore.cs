using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerScore : MonoBehaviour
{
    public UnityAction<int> OnScoreChange;
    public UnityAction<int> OnBestScoreChange;
    public UnityAction<DateTime> OnBeatDateChange;

    int score;
    int theBestScore;
    DateTime beatDate;

    private void Awake()
    {
        ReadBestScore();
    }

    public void IncrementScore()
    {
        score++;
        OnScoreChange?.Invoke(score);
        CheckBestScore(score);
    }
    void CheckBestScore(int currentScore)
    {
        if (currentScore > theBestScore)
        {
            theBestScore = currentScore;
            OnBestScoreChange?.Invoke(theBestScore);

            beatDate = DateTime.Now;
            OnBeatDateChange?.Invoke(beatDate);
        }
    }

    void ReadBestScore()
    {
        SaveData saveData = SaveSystem.LoadFromFile();
        if (saveData != null)
        {
            theBestScore = saveData.bestScore;
            OnBestScoreChange?.Invoke(theBestScore);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
