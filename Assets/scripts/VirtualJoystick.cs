using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image bgImg;
    private Image Knob;
    private Vector3 inputVector;
    private void Start()
    {
        bgImg = GetComponent<Image>();
        Knob = transform.GetChild(0).GetComponent<Image>();
    }
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2 + 1, 0, pos.y * 2 - 1);
            inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;

            Knob.rectTransform.anchoredPosition = new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 2), inputVector.z * (bgImg.rectTransform.sizeDelta.y / 2));
            Debug.Log(pos);
        }
    }
    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }
    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        Knob.rectTransform.anchoredPosition = Vector3.zero;

    }
    public float Horizontal()
    {
        if (inputVector.x != 0)
        {
            return inputVector.x / 4;
        }
        else
            return 0;
        /*else 
            return Input.GetAxis("Horizontal");*/
    }
    public float Vertical()
    {
        if (inputVector.z != 0)
        {
            return inputVector.z / 4;
        }
        else
            return 0;
        /* else 
             return Input.GetAxis("Vertical");*/
    }
}
