using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class SaveData
{
    public int bestScore;
}
public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI bestScoreText;
    [SerializeField] TextMeshProUGUI userLife;
    PlayerScore playerScore;
    PlayerLife playerLife;
    private void Awake()
    {
     
    }

    private void LifeChangeDetect(int newUserLife)
    {
        userLife.text=$"Lifes: {newUserLife}";
    }

    private void ScoreChangeDetect(int newScore)
    {
        scoreText.text = $"Current score: {newScore}";
    }

    private void BestScoreChangeDetect(int newBestScore)
    {
        bestScoreText.text = $"Best score: {newBestScore}";
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
