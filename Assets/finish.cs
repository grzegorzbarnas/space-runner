﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finish : MonoBehaviour
{
    // Start is called before the first frame update

    public static bool GameIsPaused = false;
    public GameObject PauseMenuUI;

    void OnTriggerEnter2D(Collider2D col)
    {
        
            if (col.gameObject.tag == "Finish")
            {
            PauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            GameIsPaused = true;

         }
        
    }

  
}